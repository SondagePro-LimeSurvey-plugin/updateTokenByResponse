<?php
/**
 * updateTokenByResponse
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2016 Denis Chenu <http://www.sondages.pro>
 * @copyright 2016 Yxplora AG <https://www.yxplora.ch/>

 * @license GPL v3
 * @version 0.0.2
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class updateTokenByResponse extends \ls\pluginmanager\PluginBase
{
  protected $storage = 'DbStorage';

  static protected $description = 'Update token after survey is submitted';
  static protected $name = 'updateTokenByResponse';

  protected $settings = array(
      'information' => array(
          'type' => 'info',
          'class' => "alert alert-info",
          'content' => '<p>Set the default value for each token, you can use Expression Manager, <strong>remind to use {} to activate expression manager</strong>. Expression is not validated, if there are error : this is not fixed. If the result of the expression is empty (trimmed) : attribute is not updated</p>',
      ),
      'em_token_firstname'=>array(
        'type'=>"string",
        'label'=>"Value for firstname",
        'default'=>"",
      ),
      'em_token_lastname'=>array(
        'type'=>"string",
        'label'=>"Value for lastname",
        'default'=>"",
      ),
      'em_token_email'=>array(
        'type'=>"string",
        'label'=>"Value for email",
        'default'=>"",
      ),
  );

  public function init()
  {
    $this->subscribe('afterSurveyComplete');
    $this->subscribe('beforeSurveySettings');
    $this->subscribe('newSurveySettings');
  }
  public function afterSurveyComplete()
  {
    $iSurveyId   = $this->event->get('surveyId');
    $aTokenAttributes=$this->_getTokensAttributes($iSurveyId);
    if(!empty($aTokenAttributes)) {
      $aResponse   = $this->pluginManager->getAPI()->getResponse($iSurveyId, $this->event->get('responseId'));
      if($aResponse && isset($aResponse['token']) && $aResponse['token'])// OK for anonymous token : no issue
      {
        $aAttributeUpdate=array();
        foreach($aTokenAttributes as $attribute=>$description) {
          $sEmValue=$this->get("em_token_{$attribute}","Survey",$iSurveyId,"");
          if($sEmValue==="") {
            $sEmValue=$this->get("em_token_{$attribute}",null,null,"");
          }

          if($sEmValue==="") {
            $sValue= trim(LimeExpressionManager::ProcessString($sEmValue,0,array(),0,1,1,false,false,true));// force static
            if(!empty($sValue)) { // @todo : add a settings , allow empty string + 0
              $aAttributeUpdate[$attribute]=$sValue;
            }
          }
        }

        if(!empty($aAttributeUpdate)) {
          $oToken=Token::model($iSurveyId)->find("token=:token",array(":token"=>$aResponse['token']));
          if($oToken) {
            foreach($aAttributeUpdate as $attribute=>$value) {
              $oToken->$attribute=$value;
            }
            if(!$oToken->save()) {
              Yii::log("Error when save {$aResponse['token']} for survey $iSurveyId", 'warning','application.plugins.updateTokenByResponse');
              Yii::log(CVarDumper::dumpAsString($oToken->getErrors()), 'warning','application.plugins.updateTokenByResponse');
              tracevar($oToken->getErrors());
            }
          }
        }
      }
    }
  }

    public function newSurveySettings()
    {
      /* save it direclty : maybe use array for setting token_ ? */
      foreach ($this->event->get('settings') as $name => $value) {
        $this->set($name, $value, 'Survey', $this->event->get('survey'),"");
      }
    }
    public function beforeSurveySettings()
    {
      $oEvent=$this->event;
      /* Token attribute */
      $aSettings = $this->_getSurveySettings($oEvent->get('survey'));
      if(!empty($aSettings)) {
        $oEvent->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => $aSettings,
        ));
      }
  }

  /**
   * get the token attributes array
   * @para int $iSurveyId
   * @return array
   */
  private function _getTokensAttributes($iSurveyId)
  {
    $aTokenAttributes=array();

    if($iSurveyId && tableExists("{{tokens_{$iSurveyId}}}")){
      $aRealTokenAttributes=array_keys(Yii::app()->db->schema->getTable("{{tokens_{$iSurveyId}}}")->columns);
      $aRealTokenAttributes=array_combine($aRealTokenAttributes,$aRealTokenAttributes);
      $aTokenAttributes=array_filter(Token::model($iSurveyId)->attributeLabels());

      $aTokenAttributes=array_diff_key(
          array_replace($aRealTokenAttributes,$aTokenAttributes),
          array(
              'tid' => 'tid',
              'partcipant' => 'partcipant',
              'participant' => 'participant',
              'participant_id' => 'participant_id',
              'emailstatus' => 'emailstatus',
              'token' => 'token',
              'language' => 'language',
              'blacklisted' => 'blacklisted',
              'sent' => 'sent',
              'remindersent' => 'remindersent',
              'remindercount' => 'remindercount',
              'completed' => 'completed',
              'usesleft' => 'usesleft',
              'validfrom' => 'validfrom',
              'validuntil' => 'validuntil',
              'mpid' => 'mpid'
          )
      );
    }
    return $aTokenAttributes;
  }

  /**
   * update the plugin settings when loaded
   */
  public function getPluginSettings($getValues=true)
  {
    /* Find a better way, update via javascript ? */
    for ($cnt = 1; $cnt <= 100; $cnt++) {
      $this->settings["em_token_attribute_{$cnt}"]=array(
        'type'=>"string",
        'label'=>"Value for attribute {$cnt}",
        'default'=>"",
      );
    }
    return parent::getPluginSettings($getValues);
  }

  /**
   * Get the survey settings
   * @param int $iSurveyId
   * @return array[]
   */
  private function _getSurveySettings($iSurveyId)
  {
    $aTokenAttributes=$this->_getTokensAttributes($iSurveyId);
    $aSettings = array();
    if(!empty($aTokenAttributes)) {
      $oSurvey=Survey::model()->findByPk($iSurveyId);
      foreach($aTokenAttributes as $attribute=>$description)
      {
        $default=$this->get("em_token_{$attribute}",null,null,"");
        $aSettings["em_token_{$attribute}"]=array(
            'type'=>'string',
            'label'=>"<span class='label'>".(empty($description) ? $attribute : $description)."</span>, updated with",
            'current'=>$this->get("em_token_{$attribute}","Survey",$iSurveyId,""),
        );
        $actualValue=$this->get("em_token_{$attribute}","Survey",$iSurveyId,$default);
        if($actualValue) {
           $aSettings["em_token_{$attribute}"]['help']="Actual value: ".$this->_getHtmlExpression($actualValue,$iSurveyId);
        } elseif(!empty($default)) {
          $aSettings["em_token_{$attribute}"]['help']="Actual value (by default): ".$this->_getHtmlExpression($default,$iSurveyId);
        }
      }
    }
    return $aSettings;

  }
  /**
  * Get the complete HTML from a string with Expression for admin
  * @param string $sExpression : the string to parse
  * @param array $aReplacement : optionnal array of replacemement
  * @param boolean $forceEm : force EM or not
  *
  * @author Denis Chenu
  * @version 1.0
  */
  private function _getHtmlExpression($sExpression,$iSurveyId,$aReplacement=array(),$forceEm=false)
  {
      $LEM = LimeExpressionManager::singleton();

      $aReData=array();
      if($iSurveyId) {
          $LEM::StartSurvey($iSurveyId, 'survey', array('hyperlinkSyntaxHighlighting'=>true));// replace QCODE
          $aReData['thissurvey']=getSurveyInfo($iSurveyId);
      }
      if($forceEm) {
          $sExpression = "{".$sExpression."}";
      }else{
          $oFilter = new CHtmlPurifier();
          $sExpression =$oFilter->purify(viewHelper::filterScript($sExpression));
      }
      
      templatereplace($sExpression, $aReplacement,$aReData,__CLASS__,false,null,array(),true);
      return $LEM::GetLastPrettyPrintExpression();
  }
  
}
